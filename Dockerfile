# ---- Base Node ----
FROM node:12.16.3 AS base
# Create app directory
WORKDIR /app

# ---- Dependencies ----
FROM base AS dependencies  
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./
# install app dependencies including 'devDependencies'
RUN npm install

# ---- Copy Files/Build ----
FROM dependencies AS build  
WORKDIR /app
COPY src /app

# --- Release with Alpine ----
FROM node:12.16.3-alpine AS release  
# Create app directory
WORKDIR /app
COPY --from=dependencies /app/package.json ./
# Install app dependencies
RUN npm install --only=production
# RUN npm install
COPY --from=build /app ./
# CMD ["node", "server.js"]
CMD ["npm", "start"]