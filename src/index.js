import moment, { fn } from 'moment'
import axios from 'axios'
import cron from 'node-cron'

import PostModel from './models/Post'
import app from './server'

/**
*   HN Posts server
*
*   @author Dilip Ramírez <www.linkedin.com/in/dilipramirez><https://github.com/dukuo>
*
*   Fetches Hacker News data from an Algolia API endpoint every 1 hour (configurable) 
*   and stores it in a MongoDB database.
*
*   There are two implementations for the recurring task:
*   - Timer with setInterval and
*   - A Cron task using node-cron
*   
*   Pros of 'cron': Although node-cron is a cron-like system and it's not actually using the system's crontab,
*   it has the same methods of it. The underlying process uses setTimeout with process.hrtime()'s precision. 
*   Cons: Default precision is down to 1 minute. Task is executed according to the system clock time instead of starting
*   from the time it first executed as opposed to setInterval that begins its timer when its executed.
*   
*   Pros of Timer: Simpler logic and uses Node's native setInterval. 
*   Cons: Execution delay might drift depending on server load.
*
*   A more stable and resilient way to implement this should be using crontab(1) which would survive even if this server shuts down. 
*/

require('dotenv').config()

const port = 8888
const apiUrl = process.env.HN_API_URL

/**
 * fetchHNPosts
 * @description Fetches an array of objects from API url and stores it on a Mongoose Collection
 * @param  {String} url
 * @param {Object} collection
 * @returns void
 */
export async function fetchHNPosts(url, collection) {
    // Testing: remove collection before updating.
    // Actual behaviour should check for items already in the collection and ignore them.
    // WARNING: Uncommenting the following line will drop the collection which might have values already. 
    // DO NOT USE UNLESS YOU KNOW WHAT YOU ARE DOING. You have been warned.
    // Post.collection.drop()

    console.log(`Fetching HN Posts at ${moment().format()}.`)

    const response = await axios.get(url)
    const { data: { hits } } = response
    console.log(`Got ${hits.length} records. Checking if records already exists in collection.`)
    hits.forEach(post => {
        const {
            title,
            url,
            story_text,
            story_id,
            story_title,
            story_url,
            author,
            created_at,
        } = post
        if (title || story_title) {
            const query = { story_id: story_id }
            const update = { $setOnInsert: { title, url, story_text, story_id, story_title, story_url, author, created_at, deleted: false } }
            const options = { upsert: true }
            collection.findOneAndUpdate(query, update, options, (err, rs) => {
                if (err) console.log(err)
            })
        }
    })
}

/**
 * Init Fetch Timer
 * @description Creates a timer using setInterval which loops until destroyed.
 * @param  {Function} fn
 * @param  {Number} timespan
 * @param  {Boolean} shouldExecuteAtStart
 * @returns {Object} timer - The created timer.
 */
export function initFetchTimer(fn, timespan, shouldExecuteAtStart = true) {
    // Execute immediately the first time
    if (shouldExecuteAtStart) fn()

    // Then create a timer.
    return setInterval(fn, timespan)
}

/*
    * * * * * *
     | | | | | |
     | | | | | day of week
     | | | | month
     | | | day of month
     | | hour
     | minute
     second ( optional )
*/
/**
 * Set Cron Task
 * @description Creates a cron-like task using node-cron. Note: This is NOT a substitute for *nix crontab(1).
 * @param  {String} exp - Cron Expression (see diagram above)
 * @param  {Function} fn - The function to execute
 * @returns {Object} cron - The newly created Cron job.
 */
export function setCron(exp, fn) {
    // Execute immediately the first time
    fn()

    return cron.schedule(exp, fn)
}

/**
 * Destroy Cron Job
 * @param  {Object} cronJob
 * @returns void
 */
export function destroyCron(cronJob) {
    return cronJob.destroy()
}

/**
 * Initialize Server
 * @description Starts an Express server on PORT.
 * @param  {Number} port
 * @returns void
 */
export async function initServer(port) {
    app.listen(port, () => console.log(`HN Post Server started at ${port}.`))
}

// Timer method
initFetchTimer(() => fetchHNPosts(apiUrl, PostModel), 3600 * 1000)

// Cron Method
// const task = setCron('0 * * * *', fetchHNPosts)
// process.on('exit', () => destroyCron(task))

initServer(port)
    .catch(e => console.log(e))
