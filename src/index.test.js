import chai, { assert, expect } from 'chai'
// import assert from 'assert'
import request from 'supertest'

import app from './server'
import { initFetchTimer, fetchHNPosts } from './index'
import PostModel from './models/Post'

require('dotenv').config()

const port = 8888

if (!module.parent) {
    app.listen(port)
}

chai.should()

describe("Mongoose Models", () => {
    describe("Post Model", () => {
        it("Should be an object", done => {
            const m = new PostModel()
            expect(m).to.be.a('object')
            done()
        })
    })
    describe("New Dummy Post", () => {
        it("Should have a title equal to 'Something'", done => {
            const data = {
                title: 'Something'
            }
            const newPost = new PostModel(data)
            expect(newPost.title).to.be.a('string')
            expect(newPost.title).to.equal('Something')
            done()
        })
    })
})

describe("HN Posts Server", () => {
    describe("API: 'Posts' Router", () => {
        it("GET /post Should return HTTP Status Code 200 OK and an array", done => {
            request(app)
                .get('/post')
                .expect(200)
                // .end((err, res) => {
                //     if (err) done(err)
                //     res.status.should.equal(200)
                //     res.body.should.be.an('array')
                //     done()
                // })
                done()
        })
        it("DELETE /post/123 should return HTTP Status Code 404 Not Found", done => {
            request(app)
                .delete('/post/123')
                .end((err, res) => {
                    if (err) done(err)
                    res.status.should.equal(404)
                    done()
                })
        })
        it("POST /post should return HTTP Status Code 501 Not Implemented", done => {
            request(app)
                .post('/post')
                .end((err, res) => {
                    if(err) done(err)
                    res.status.should.equal(501)
                    done()
                })
        })
        it("PUT /post should return HTTP Status Code 501 Not Implemented", done => {
            request(app)
                .put('/post')
                .end((err, res) => {
                    if(err) done(err)
                    res.status.should.equal(501)
                    done()
                })
        })
    })
    describe("Fetch API Logic", () => {
        describe("Fetch Post Interval", () => {
            it("initFetchTimer should return a timer", done => {
                const timer = initFetchTimer(() => { }, 3600 * 1000)
                assert.isObject(timer)
                done()
            })
        })
    })
})

