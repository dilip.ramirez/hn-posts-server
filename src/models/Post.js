import mongoose, { Schema } from 'mongoose'

const ObjectId = Schema.ObjectId

const postSchema = new Schema({
    _id: ObjectId,
    title: String,
    url: String,
    story_text: String,
    story_id: Number,
    story_title: String,
    story_url: String,
    author: String,
    created_at: Date,
    deleted: { type: Boolean, default: false },
})

export default mongoose.model('Post', postSchema)