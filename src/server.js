import express from 'express'
import cors from 'cors'
import mongoose from 'mongoose'
import api from './api'

require('dotenv').config()

/**
 * Express Server App
 * @description Creates an Express App with custom routers
 * 
 */

const app = express()
const mongoApiUrl = process.env.MONGODB_API_URL

app.use(express.json())
app.use(cors())

mongoose.connect(mongoApiUrl)
mongoose.set('useFindAndModify', false);

app.use('/post', api.routers.post)

export default app