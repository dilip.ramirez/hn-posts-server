import { Router } from 'express'

import Post from '../../../models/Post'

const postRouter = Router()

postRouter.get('/', (req, res) => {
    Post.count((err, count) => {
        if (count > 0) {
            Post.find({ deleted: false })
                .sort({ date: 'asc'})
                .exec((err, posts) => {
                    if (err) res.sendStatus(502)
                    if (posts.length > 0) {
                        res.send(posts)
                    } else {
                        res.sendStatus(204)
                    }
                })
        } else {
            res.send([])
        }
    })
})

postRouter.post('/', (req, res) => {
    res.sendStatus(501)
})

postRouter.delete('/:id', (req, res) => {
    const { id } = req.params

    const query = { _id: id }
    const update = { deleted: true }

    Post.count(query, (err, count) => {
        if (count > 0) {
            Post.findOneAndUpdate(query, update, (err, rs) => {
                console.log("Record Updated.")
                if (!err) return res.sendStatus(200)
                return res.sendStatus(204)
            })
        } else {
            return res.sendStatus(404)
        }
    })
})

postRouter.put('/', (req, res) => {
    res.sendStatus(501)
})

export default postRouter